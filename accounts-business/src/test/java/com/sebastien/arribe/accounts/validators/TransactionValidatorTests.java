package com.sebastien.arribe.accounts.validators;
import com.sebastien.arribe.accounts.beans.Transaction;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionValidatorTests {

    private static Transaction transactionEmitterIsRecipient;
    private static Transaction transactionAmountIsZero;
    private static Transaction transactionOk;

    private static TransactionsValidator transactionsValidator = new TransactionsValidator();



    {
        transactionEmitterIsRecipient =new Transaction();
        transactionEmitterIsRecipient.setEmitter(2);
        transactionEmitterIsRecipient.setRecipient(2);
        transactionEmitterIsRecipient.setAmount(123);

        transactionAmountIsZero =new Transaction();
        transactionAmountIsZero.setEmitter(2);
        transactionAmountIsZero.setRecipient(1);
        transactionAmountIsZero.setAmount(0);

        transactionOk =new Transaction();
        transactionOk.setEmitter(2);
        transactionOk.setRecipient(1);
        transactionOk.setAmount(123);
    }

    @Test
    public void validateTransactionClassNoError (){
        assertTrue(transactionsValidator.supports(Transaction.class));
    }

    @Test
    public void invalidateStringClassError (){
        assertFalse(transactionsValidator.supports(String.class));
    }

    @Test
    public void validateTransactionOK (){
        Errors errors = new BeanPropertyBindingResult(transactionOk, "transactionOk");
        transactionsValidator.validate(transactionOk, errors);
        assertFalse(errors.hasErrors());
    }

    @Test
    public void invalidateEmitterIsRecipient (){
        Errors errors = new BeanPropertyBindingResult(transactionEmitterIsRecipient, "transactionEmitterIsRecipient");
        transactionsValidator.validate(transactionEmitterIsRecipient, errors);
        assertTrue(errors.hasErrors());
        assertTrue(errors.hasFieldErrors("emitter"));
    }

    @Test
    public void invalidateAmountIsZero (){
        Errors errors = new BeanPropertyBindingResult(transactionAmountIsZero, "transactionAmountIsZero");
        transactionsValidator.validate(transactionAmountIsZero, errors);
        assertTrue(errors.hasErrors());
        assertTrue(errors.hasFieldErrors("amount"));
    }

}
