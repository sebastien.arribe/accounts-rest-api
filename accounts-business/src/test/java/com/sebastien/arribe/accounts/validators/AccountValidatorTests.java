package com.sebastien.arribe.accounts.validators;

import com.sebastien.arribe.accounts.beans.Account;
import com.sebastien.arribe.accounts.beans.Transaction;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class AccountValidatorTests {

    private static Account accountBalanceOK;
    private static Account accountBalanceKO;

    private static AccountsValidator accountsValidator = new AccountsValidator(new TransactionsValidator());

    {

        Transaction transaction0 = new Transaction();
        transaction0.setAmount(100);
        transaction0.setRecipient(0);
        transaction0.setEmitter(1);

        Transaction transaction1 = new Transaction();
        transaction1.setAmount(100);
        transaction1.setRecipient(1984);
        transaction1.setEmitter(0);

        Transaction transaction2 = new Transaction();
        transaction2.setAmount(1023);
        transaction2.setRecipient(1);
        transaction2.setEmitter(0);

        Transaction transaction3 = new Transaction();
        transaction3.setAmount(42);
        transaction3.setRecipient(12);
        transaction3.setEmitter(1);

        List <Transaction> transactionsAccount0 = new ArrayList<>();
        transactionsAccount0.add(transaction0);
        transactionsAccount0.add(transaction1);
        transactionsAccount0.add(transaction2);


        List <Transaction> transactionsAccount1 = new ArrayList<>();
        transactionsAccount1.add(transaction0);
        transactionsAccount1.add(transaction2);
        transactionsAccount1.add(transaction3);

        accountBalanceOK = new Account();
        accountBalanceOK.setId(0);
        accountBalanceOK.setName("John");
        accountBalanceOK.setSurname("Stewart");
        accountBalanceOK.setBalance(-1023);
        accountBalanceOK.setTransactions(transactionsAccount0);

        accountBalanceKO = new Account();
        accountBalanceKO.setId(1);
        accountBalanceKO.setName("Jon");
        accountBalanceKO.setSurname("Oliver");
        accountBalanceKO.setBalance(123);
        accountBalanceKO.setTransactions(transactionsAccount1);

    }

    @Test
    public void validateTransactionClassNoError (){
        assertTrue(accountsValidator.supports(Account.class));
    }

    @Test
    public void invalidateStringClassError (){
        assertFalse(accountsValidator.supports(String.class));
    }

    @Test
    public void validateBalanceOK (){
        Errors errors = new BeanPropertyBindingResult(accountBalanceOK, "accountBalanceOK");
        accountsValidator.validate(accountBalanceOK, errors);
        assertFalse(errors.hasErrors());
    }

    @Test
    public void invalidateBalanceKO (){
        Errors errors = new BeanPropertyBindingResult(accountBalanceKO, "accountBalanceKO");
        accountsValidator.validate(accountBalanceKO, errors);
        assertTrue(errors.hasErrors());
        assertTrue(errors.hasFieldErrors("balance"));
    }


}
