package com.sebastien.arribe.accounts.validators;

import com.sebastien.arribe.accounts.beans.Account;
import com.sebastien.arribe.accounts.beans.Transaction;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class AccountsValidator implements Validator {

    private TransactionsValidator transactionsValidator;

    private static final String ACCOUNT_IS_NEITHER_EMITTER_OR_RECIPIENT_ERROR = "Acount with id: %1$s is neither emitter or recipient of transaction with id: %2$s";
    private static final String BALANCE_NOT_CONSISTENT_ERROR = "Balance (%1$s) of account with id: %2$s is not consistent with the amount of transactions (%3$s)";

    public AccountsValidator(TransactionsValidator transactionValidator) {
        this.transactionsValidator = transactionValidator;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Account.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        Account account = (Account) target;

        validateBalanceAndTransactions(account, errors);

    }

    /**
     * According to the requirements (SPEC number)
     * if initialCredit is not 0, a transaction will be sent to the new account
     */

    /**
     * Checks
     *
     <ul>
         <li>
             if the balance is equals to the sum of transactions (de facto checks ${SPEC_NUMBER} :
             if initialCredit is not 0, a transaction will be sent to the new account
         </li>
         <li>if the account is either emitter or recipient of each transaction</li>
     </ul>
     *  -
     *
     * @param account
     * @param errors
     */
    private void validateBalanceAndTransactions(Account account, Errors errors) {

        int balanceFromTransactions = 0;

        //should not happen because account.transactions is always instantiated
        // but better safe than sorry
        if (account.getTransactions() != null) {

            for (Transaction t : account.getTransactions()) {

                transactionsValidator.validate(t, errors);

                if (t.getEmitter() == account.getId()) {
                    balanceFromTransactions -= t.getAmount();
                } else if (t.getRecipient() == account.getId()) {
                    balanceFromTransactions += t.getAmount();
                } else {
                    //dead code?
                    errors.reject(String.format(ACCOUNT_IS_NEITHER_EMITTER_OR_RECIPIENT_ERROR, account.getId(), t.getId()));
                }
            }

            if (balanceFromTransactions != account.getBalance()) {
                errors.rejectValue("balance", String.format(BALANCE_NOT_CONSISTENT_ERROR, account.getBalance(), account.getId(), balanceFromTransactions));
            }

        }
    }

}
