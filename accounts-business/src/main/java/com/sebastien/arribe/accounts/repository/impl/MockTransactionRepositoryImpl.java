package com.sebastien.arribe.accounts.repository.impl;

import com.sebastien.arribe.accounts.beans.Transaction;
import com.sebastien.arribe.accounts.repository.TransactionRepository;
import org.springframework.stereotype.Repository;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Repository("mockTransactionRepositoryImpl")
@Singleton
public class MockTransactionRepositoryImpl implements TransactionRepository {


    private  List<Transaction> transactionsList;


    public MockTransactionRepositoryImpl() {

        Transaction transaction0 = new Transaction();
        Transaction transaction1 = new Transaction();
        Transaction transaction2 = new Transaction();
        Transaction transaction3 = new Transaction();

        transaction0.setId(0);
        transaction0.setAmount(1000);
        transaction0.setEmitter(0);
        transaction0.setRecipient(2);

        transaction1.setId(1);
        transaction1.setAmount(10);
        transaction1.setEmitter(0);
        transaction1.setRecipient(4);

        transaction2.setId(2);
        transaction2.setAmount(0);
        transaction2.setEmitter(0);
        transaction2.setRecipient(1);

        transaction3.setId(3);
        transaction3.setAmount(60);
        transaction3.setEmitter(2);
        transaction3.setRecipient(2);

        transactionsList = new ArrayList<>();
        transactionsList.add(transaction0);
        transactionsList.add(transaction1);
        transactionsList.add(transaction2);
        transactionsList.add(transaction3);

    }


    @Override
    public Transaction getTransactionById(int id) {
        for (Transaction t : transactionsList){
            if (t.getId().equals(id)){
                return t;
            }
        }
        return null;
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return this.transactionsList;
    }

    public List<Transaction> getTransactionsList() {
        return this.transactionsList;
    }

    public void setTransactionsList(List<Transaction> transactionsList) {
        this.transactionsList = transactionsList;
    }
}
