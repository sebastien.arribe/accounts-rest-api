package com.sebastien.arribe.accounts.validators;

import com.sebastien.arribe.accounts.beans.Transaction;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class TransactionsValidator  implements Validator {

    private static final String TRANSACTION_AMOUNT_CANNOT_BE_ZERO_ERROR = "Transaction of id: %1$s has an amount of 0";
    private static final String EMITTER_IS_RECIPIENT_ERROR = "Transaction of id:  %1$s has the same emitter as recipient";

    @Override
    public boolean supports(Class<?> clazz) {
        return Transaction.class.equals(clazz);

    }

    @Override
    public void validate(Object target, Errors errors) {

        Transaction transaction = (Transaction) target;

        if (transaction.getRecipient() == transaction.getEmitter()){
            errors.rejectValue( "emitter", String.format(EMITTER_IS_RECIPIENT_ERROR, transaction.getId(), transaction.getEmitter() ));
        }

        if (transaction.getAmount() == 0){
            errors.rejectValue("amount", String.format(TRANSACTION_AMOUNT_CANNOT_BE_ZERO_ERROR, transaction.getId()) );
        }

    }
}
