package com.sebastien.arribe.accounts.services;

import com.sebastien.arribe.accounts.repository.TransactionRepository;
import com.sebastien.arribe.accounts.beans.Transaction;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Service for transactions.
 */
@Service
@DependsOn("mockAccountRepositoryImpl")
public class TransactionService {


    @Inject
    @Qualifier("mockTransactionRepositoryImpl")
    public TransactionRepository transactionRepository;

    /**
     * static factory to simplify bean instantiation
     *
     * @Param id of emitter account
     * @Param id of recipient account
     * @Param amount
     */
    public static Transaction createTransactionFromIdsAndAmount(int emitterId, int recipientId, int amount) {
        Transaction transaction = new Transaction();

        transaction.setEmitter(emitterId);
        transaction.setRecipient(recipientId);
        transaction.setAmount(amount);

        return transaction;
    }

    public Transaction getTransactionById(int id) {
        return this.transactionRepository.getTransactionById(id);
    }

    public List<Transaction> getAllTransactions() {
        return this.transactionRepository.getAllTransactions();
    }

}
