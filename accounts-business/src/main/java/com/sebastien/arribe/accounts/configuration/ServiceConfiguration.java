package com.sebastien.arribe.accounts.configuration;

import com.sebastien.arribe.accounts.repository.AccountsRepository;
import com.sebastien.arribe.accounts.repository.TransactionRepository;
import com.sebastien.arribe.accounts.repository.impl.MockAccountRepositoryImpl;
import com.sebastien.arribe.accounts.repository.impl.MockTransactionRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    @Bean
    public TransactionRepository transactionRepository (){
        return new MockTransactionRepositoryImpl();

    }

    //mandatory to inject transactionRepository in accountRepository
    //otherwise -> NPE
    @Bean
    public AccountsRepository accountsRepository(){
        return new MockAccountRepositoryImpl(transactionRepository ());

    }

}
