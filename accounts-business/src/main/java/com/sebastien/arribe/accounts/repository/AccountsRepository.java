package com.sebastien.arribe.accounts.repository;

import com.sebastien.arribe.accounts.beans.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountsRepository {

    public List<Account> getAllAccounts();

    public Account getAccountById(int id);

    public void saveAccount(Account account);


}
