package com.sebastien.arribe.accounts.repository.impl;

import com.sebastien.arribe.accounts.beans.Account;
import com.sebastien.arribe.accounts.repository.AccountsRepository;

import java.util.List;

/**
 * To Implement when using a DB
 */
public class DBAccountRepositoryImpl implements AccountsRepository {
    @Override
    public List<Account> getAllAccounts() {
        throw new UnsupportedOperationException("TODO: Implement the real Repository");
    }

    @Override
    public Account getAccountById(int id) {
        throw new UnsupportedOperationException("TODO: Implement the real Repository");
    }

    @Override
    public void saveAccount(Account account) {
        throw new UnsupportedOperationException("TODO: Implement the real Repository");
    }
}
