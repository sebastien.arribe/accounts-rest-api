package com.sebastien.arribe.accounts.beans;

public class Transaction {

    private Integer id;

    private int emitter;

    private int recipient;

    private int amount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getEmitter() {
        return emitter;
    }

    public void setEmitter(int emitter) {
        this.emitter = emitter;
    }

    public int getRecipient() {
        return recipient;
    }

    public void setRecipient(int recipient) {
        this.recipient = recipient;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
