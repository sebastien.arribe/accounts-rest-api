package com.sebastien.arribe.accounts.repository.impl;

import com.sebastien.arribe.accounts.repository.TransactionRepository;
import com.sebastien.arribe.accounts.beans.Account;
import com.sebastien.arribe.accounts.beans.Transaction;
import com.sebastien.arribe.accounts.repository.AccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Repository
@DependsOn({"mockTransactionRepositoryImpl"})
@Singleton
public class MockAccountRepositoryImpl implements AccountsRepository {

    @Autowired
    private TransactionRepository transactionRepository;

    private static List<Account> accountsList;

    public MockAccountRepositoryImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;

        List<Transaction> transactionList = transactionRepository.getAllTransactions();

        //fake data for Mock
        Account account0 = new Account();
        account0.setId(0);
        account0.setName("Bank");
        account0.setSurname("");
        account0.setBalance(1000000);
        account0.getTransactions().add(transactionList.get(0));
        account0.getTransactions().add(transactionList.get(1));

        Account account1 = new Account();
        account1.setId(1);
        account1.setName("Isaac");
        account1.setSurname("Asimov");
        account1.setBalance(2);
        account1.getTransactions().add(transactionList.get(2));

        Account account2 = new Account();
        account2.setId(2);
        account2.setName("Martha");
        account2.setSurname("Wells");
        account2.setBalance(5);
        account2.getTransactions().add(transactionList.get(0));
        account2.getTransactions().add(transactionList.get(2));
        account2.getTransactions().add(transactionList.get(3));

        Account account3 = new Account();
        account3.setId(3);
        account3.setName("Georges");
        account3.setSurname("Orwell");
        account3.setBalance(1984);

        Account account4 = new Account();
        account4.setId(4);
        account4.setName("Dan");
        account4.setSurname("Abnett");
        account4.getTransactions().add(transactionList.get(1));
        account4.getTransactions().add(transactionList.get(3));


        this.accountsList = new ArrayList<>();
        accountsList.add(account0);
        accountsList.add(account1);
        accountsList.add(account2);
        accountsList.add(account3);
        accountsList.add(account4);

    }


    @Override
    public List<Account> getAllAccounts(){

        return accountsList;
    }

    @Override
    public Account getAccountById(int id){
        for (Account a : accountsList){
                if (a.getId().equals(id)){
                    return a;
                }
        }
        return null;
    }

    @Override
    public void saveAccount (Account account){
        accountsList.add(account);
    }

}
