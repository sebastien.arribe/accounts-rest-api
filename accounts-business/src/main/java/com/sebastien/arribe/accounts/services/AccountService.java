package com.sebastien.arribe.accounts.services;

import com.sebastien.arribe.accounts.repository.AccountsRepository;
import com.sebastien.arribe.accounts.beans.Account;
import com.sebastien.arribe.accounts.beans.Transaction;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
/**
 * Service for accounts.
 */
@Service
@DependsOn("transactionService")
public class AccountService {

    @Inject
    @Qualifier("mockAccountRepositoryImpl")
    private AccountsRepository accountsRepository;

    public Account getAccountById(int id) {

        Account account = this.accountsRepository.getAccountById(id);

        if (account != null){
            return account;
        }else {
            throw new IllegalArgumentException("There is no account with this id.");
        }

    }

    public List<Account> getAllAccounts (){
        return this.accountsRepository.getAllAccounts();
    }

    public void createNewAccount(Account account){
        accountsRepository.saveAccount(account);
    }

    public void sendTransaction(Transaction transaction){
        Account emitter = this.getAccountById(transaction.getEmitter());
        Account recipient = this.getAccountById(transaction.getRecipient());

        //be careful of the signs and instances names!
        emitter.setBalance(emitter.getBalance() - transaction.getAmount());
        recipient.setBalance(recipient.getBalance() + transaction.getAmount());

        emitter.getTransactions().add(transaction);
        recipient.getTransactions().add(transaction);

    }


}
