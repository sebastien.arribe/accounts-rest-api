package com.sebastien.arribe.accounts.repository;

import com.sebastien.arribe.accounts.beans.Transaction;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface TransactionRepository {

    public Transaction getTransactionById (int id);

    public List<Transaction> getAllTransactions();

}
