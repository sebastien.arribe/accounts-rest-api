package com.sebastien.arribe.accounts.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the accounts.
 *
 * Id is its unique identifier.
 * All transactions concerning the account are listed in <b>transaction</b>, whether the account is emitter or recipient.
 */
public class Account {

    private Integer id;

    private String name;

    private String surname;

    private int balance;

    private List<Transaction> transactions = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
