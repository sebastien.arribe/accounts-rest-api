# Accounts

Showcase project for managing user bank accounts and their transactions.

## Description
The project is a REST API that allows the user to create accounts with a balance and a list of transaction.
Some verifications are made (ie: the sum of transactions sent and received are equal to the balance of the account).

## Installation
The project is SpringBoot based; to run it: 

### Clone the Project
clone the repository: 
`git clone https://gitlab.com/sebastien.arribe/accounts-rest-api.git`

### Run the Project Locally 
1. go to the repository accounts-rest-api

2. run the container:
`./mvnw spring-boot:run`

## Usage
Once the backend is running, a description of the endpoints can be found on the swagger:
- http://localhost:8080/swagger-ui/index.html
- http://localhost:8080/v3/api-docs

You can use the exemples to test the API.

## License
Can be used for educationnal purposes only.
May not be sold.
