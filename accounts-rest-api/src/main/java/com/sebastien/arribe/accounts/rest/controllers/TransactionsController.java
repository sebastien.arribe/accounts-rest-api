package com.sebastien.arribe.accounts.rest.controllers;

import com.sebastien.arribe.accounts.beans.Transaction;
import com.sebastien.arribe.accounts.services.TransactionService;
import com.sebastien.arribe.accounts.validators.TransactionsValidator;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@RestController
@ComponentScan(basePackages = {"com.blue.harvest"})
public class TransactionsController {

    private static final String TRANSACTIONS_TAG = "Transactions";

    @InitBinder(value="Transaction")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new TransactionsValidator());
    }

    @Inject
    private TransactionService transactionService;

    @GetMapping("/transactions")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get all transactions",
            method = "GET",
            tags = {TRANSACTIONS_TAG},
            description = "Displays all transactions in a JSON format")
    public List<Transaction> listAllTransactions() {

        return transactionService.getAllTransactions();
    }


    @GetMapping("/transactions/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get a transaction",
            method = "GET",
            tags = {TRANSACTIONS_TAG},
            description = "Displays one transaction in a JSON format")
    @Parameter(description = "Id of the transaction to retrieve", name = "id")
    public Transaction displayOneTransaction(@PathVariable("id") int id) {
        return transactionService.getTransactionById(id);
    }

    //TODO Implement
//    @PostMapping(value="/transactions", consumes = {"application/JSON"})
    public void createTransaction(@RequestBody @Valid Transaction transaction) {
//        transactionService.sendTransaction(transaction);

    }


}
