package com.sebastien.arribe.accounts.rest.controllers;

import com.sebastien.arribe.accounts.beans.Account;
import com.sebastien.arribe.accounts.services.AccountService;
import com.sebastien.arribe.accounts.validators.AccountsValidator;
import com.sebastien.arribe.accounts.validators.TransactionsValidator;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.List;

@RestController
@ComponentScan(basePackages = "com.sebastien.arribe.accounts")
@CrossOrigin(origins = "http://localhost:4200")
public class AccountsController {

    private static final String ACCOUNTS_TAG = "Accounts";

    @InitBinder(value="account")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new AccountsValidator(new TransactionsValidator()));
    }

    @Inject
    private AccountService accountService;

    @GetMapping("/accounts")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Retrieves all accounts",
            method = "GET",
            tags = {ACCOUNTS_TAG},
            description = "Displays all accounts in a JSON format")
    public List<Account> listAllAccounts() {

        return accountService.getAllAccounts();
    }

    @GetMapping("/accounts/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get an account",
            method = "GET",
            tags = {ACCOUNTS_TAG},
            description = "Displays one account in a JSON format")
    @Parameter (description = "Id of the account to retrieve", name = "id")
    public ResponseEntity displayOneAccount(@PathVariable("id") int id) {

        try {
            return ResponseEntity.status(HttpStatus.OK).body(accountService.getAccountById(id));
        }catch (IllegalArgumentException iae){
            return ResponseEntity.badRequest().body(iae.getMessage());
        }
    }

    @PostMapping("/accounts/add")
    @Consumes (MediaType.APPLICATION_JSON)
    @Operation(summary = "Add an account",
            tags = {ACCOUNTS_TAG})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Account created!"),
            @ApiResponse(responseCode = "400", description = "One of the parameter is invalid.")
    })
    public ResponseEntity<String> createTransaction(@RequestBody @Valid Account account, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            // Extract and handle validation errors
            String errorMessage = bindingResult.getAllErrors().get(0).getCode();
            return ResponseEntity.badRequest().body(errorMessage);
        }
        accountService.createNewAccount(account);
        return  ResponseEntity.created(URI.create("/accounts/" + account.getId())).body("Account created");
    }

}
