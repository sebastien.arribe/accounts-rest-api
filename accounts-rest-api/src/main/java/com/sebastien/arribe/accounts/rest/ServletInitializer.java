package com.sebastien.arribe.accounts.rest;

import com.sebastien.arribe.accounts.rest.controllers.AccountsController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletInitializer extends SpringBootServletInitializer {
	@Autowired
	private ApplicationContext applicationContext;
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		String[] beanNames = applicationContext.getBeanDefinitionNames();
		for (String beanName : beanNames) {
			System.out.println("Bean '" + beanName + "' has been initialized.");
		}
		return application.sources(AccountsController.class);
	}

}
