package com.sebastien.arribe.accounts.rest;

import com.sebastien.arribe.accounts.rest.controllers.AccountsController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;

@SpringBootApplication
@ComponentScan(basePackageClasses= AccountsController.class)
public class AccountsRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountsRestApiApplication.class, args);
	}

}
